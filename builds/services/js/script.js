$(document).ready(function(){

$(".login-page").height($(window).innerHeight());


// Input Effect
$(function () {
	$('.input--focus').focusout(function () {
		var text_val = $(this).val();
		if (text_val === "") {
			$(this).removeClass('active-input');
		} else {
			$(this).addClass('active-input');
		}
	}).focusout();
});
// End of Input Effect


// Edit Buttons
var edit = $(".about__element--sec .ion-edit")

$(edit).click(function() {
	$(this).parents(".about__element").siblings().find(".form--edit").removeClass("form--popup");
	$(this).parents(".about__element--container").siblings(".form--edit").addClass("form--popup");
});


$(".cancel").click(function(){
	$(this).parents(".form--edit").removeClass("form--popup");
});


$(".mobile-edit-ion").click(function(){
	$(".about__element--container").hide();
	$(".service-type").hide();
	$(".form--edit").fadeIn(500);
	$(".save-mobile-changes").fadeIn(500).css('display', 'inline-block');
	$(".save-mobile-changes .cancel").fadeIn(500);
	$(this).hide();

});

$(".save-mobile-changes .cancel").click(function() {
	$(".about__element--container").fadeIn(500);
	$(".form--edit").hide();
	$(".save-mobile-changes").hide();
	$(".mobile-edit-ion").fadeIn(500);
	$(this).hide();
});
// End of Edit Buttons

// Swipe Effect
var currentSlide = 0;

$(function(){
	$(".content-container").swipe({
		swipe: function(event, direction, duration, fingerCount) {
			switch(direction) {
				case "left":
				next();
				break;
				case "right":
				back();
				break;

			}
		},
		excludedElements: "label, button, input, select, textarea, .noSwipe",
		allowPageScroll:"vertical"
	})
	initSlides();
});

function initSlides() {
	$('.tab-page').eq(currentSlide).addClass('active');
	$('.menu ul li').eq(currentSlide).addClass('active');
}

function next() {
	goto(currentSlide+1);
}

function back() {
	goto(currentSlide-1)
}

function goto(n) {
	if(n > -1 && n < $('.tab-page').length) currentSlide = n;
	else return;
	$('.tab-page').removeClass('active').eq(currentSlide).addClass('active');
	$('.menu ul li').removeClass('active').eq(currentSlide).addClass('active');
}
// End of Swipe Effect

});